cmake_minimum_required(VERSION 3.5)

set(PROJECT_NAME ark_rm)
project(${PROJECT_NAME})


##----------------------------------------------------------------------------##
## Compiler Definitions                                                       ##
##----------------------------------------------------------------------------##
set(CMAKE_CXX_STANDARD          14)
set(CMAKE_CXX_STANDARD_REQUIRED on)

##----------------------------------------------------------------------------##
## ark_core                                                                   ##
##----------------------------------------------------------------------------##
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/libs/ark_core)


##----------------------------------------------------------------------------##
## Project                                                                    ##
##----------------------------------------------------------------------------##
file(GLOB_RECURSE
  PROJECT_SOURCE_FILES
  LIST_DIRECTORIES false
  ${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}/**
)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
)
set(PROJECT_FILES
    ${PROJECT_SOURCE_FILES}
)

##----------------------------------------------------------------------------##
## Executable Definitions                                                     ##
##----------------------------------------------------------------------------##
add_executable(${PROJECT_NAME} ${PROJECT_FILES})
target_link_libraries(${PROJECT_NAME} PUBLIC ark_core)
