// Arkadia
#include "ark_core/ark_core.hpp"
// ark_rm
#include "Version.hpp"


//
// Helper Functions
//
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
ark_internal_function void
ShowHelp(ark::String const &help_string)
{
    ark::PrintLn("{}", help_string);
    ark::Exit(0);
}

//------------------------------------------------------------------------------
ark_internal_function void
ShowVersion()
{
    ark::PrintLn(
"{} - {} - stdmatt <stdmatt@pixelwizards.io>\n"
"Copyright (c) {} - stdmatt\n"
"This is a free software (GPLv3) - Share/Hack it\n"
"Check http://stdmatt.com for more :)\n",
    BuildInfo_ark_rm.ProgramName,
    BuildInfo_ark_rm.BuildSimpleVersionString  (),
    BuildInfo_ark_rm.BuildSimpleCopyrightString()
);

    ark::Exit(0);
}

//------------------------------------------------------------------------------
ark_internal_function void
RemoveFile(ark::String const &path, bool const force, bool const verbose)
{
    if(verbose) {
        ark::PrintLn("Removing file: {}", path);
    }

    if(ark::PathUtils::IsFile(path)) {
        ark::PathUtils::DeleteFileResult_t const result = ark::PathUtils::DeleteFile(path);;
        if(result.HasFailed()) {
            Fatal(
                "Failed to delete file.\nPath: {}\nError: Code {} - Msg: {}",
                path,
                result.GetError().error_code,
                result.GetError().error_msg
            );
        }
    } else if(!force){
        Fatal("Invalid Path - ({})", path);
    }
}

//------------------------------------------------------------------------------
ark_internal_function void
RemoveDir(ark::String const &path, bool const recurse, bool const force, bool const verbose)
{
    if(verbose) {
        ark::PrintLn("Removing dir: {}", path);
    }

    ark::Array<ark::String> files, dirs;
    ark::PathUtils::ListEntries(path, &files, &dirs);
    if((files.Count() + dirs.Count() != 0) && !recurse) {
        Fatal("Can't remove non empty directory - Aborting...\n   Path: ({})", path);
    }

    for(ark::String const &curr_path : files) {
        ark::String const fullpath = ark::PathUtils::Join(path, curr_path);
        RemoveFile(fullpath, recurse, verbose);
    }
    for(ark::String const &curr_path : dirs) {
        ark::String const fullpath = ark::PathUtils::Join(path, curr_path);
        RemoveDir(fullpath, recurse, force, verbose);
    }

    ark::PathUtils::DeleteDirResult_t const result = ark::PathUtils::DeleteDir(path);
    if(result.HasFailed()) {
        ark::Fatal(
            "Failed to delete directory.\nPath: {}\nError: Code {} - Msg: {}",
            path,
            result.GetError().error_code,
            result.GetError().error_msg
        );
    }
}


//
// Entry Point
//
//------------------------------------------------------------------------------
i32
main(int const argc, char const *argv[])
{
    namespace CMD = ark::CMD;
    CMD::Set(argc, argv);

    CMD::Parser parser;
    CMD::Argument const *help_arg    = parser.CreateArgument("h", "help",     "Show this screen",                                  CMD::Argument::RequiresNoValues);
    CMD::Argument const *version_arg = parser.CreateArgument("v", "version",  "Show version and copyright info",                   CMD::Argument::RequiresNoValues);
    CMD::Argument const *recurse_arg = parser.CreateArgument("r", "recursive","Remove directories and their contents recursively", CMD::Argument::RequiresNoValues);
    CMD::Argument const *force_arg   = parser.CreateArgument("f", "force",    "Ignore nonexistent files, never prompt",            CMD::Argument::RequiresNoValues);
    CMD::Argument const *verbose_arg = parser.CreateArgument("V", "verbose",  "List the operations",                               CMD::Argument::RequiresNoValues);

    CMD::Parser::ParseResult_t const parse_result = parser.Evaluate();
    if(parse_result.HasFailed()) {
        Fatal(parse_result.GetError().error_msg);
    }

    if(help_arg->WasFound()) {
        ShowHelp(parser.GenerateHelpString());
    } else if(version_arg->WasFound()) {
        ShowVersion();
    }

    ark::Array<ark::String> const &paths = parser.GetPositionalValues();
    if(paths.IsEmpty()) {
        ark::Fatal("Missing paths - Aborting...");
    }

    bool const recurse = recurse_arg->WasFound();
    bool const force   = force_arg  ->WasFound();
    bool const verbose = verbose_arg->WasFound() || true; // @todo(stdmatt): Remove it when we are sure that program is working correctly!

    for(ark::String const &curr_path : paths) {
        ark::String const fullpath = ark::PathUtils::Canonize(curr_path);
        if(ark::PathUtils::IsDir(fullpath)) {
            RemoveDir(fullpath, recurse, force, verbose);
        } else if(ark::PathUtils::IsFile(fullpath)) {
            RemoveFile(fullpath, force, verbose);
        } else if(!force_arg->WasFound()){
            ark::Fatal("Invalid Path - ({})", curr_path);
        }
    }

    return 0;
}
